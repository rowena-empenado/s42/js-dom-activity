/* 
    ACTIVITY - Session 42 
    
*/


const txtFirstName = document.querySelector("#txt-first-name");

const txtLastName = document.querySelector("#txt-last-name");

const spanFullName = document.querySelector("#span-full-name");

function showFullName () {
    spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
}

txtFirstName.addEventListener('keyup', showFullName);

txtLastName.addEventListener('keyup', showFullName);


